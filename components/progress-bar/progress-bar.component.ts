import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import cx from 'classnames';
import { Classes, Intent } from 'ng-blueprint/core/css';
import { InputBoolean } from 'ng-blueprint/core/utils';

@Component({
  selector: 'bp-progress-bar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  template: `
    <div [class]="Classes.PROGRESS_METER" [ngStyle]="progressMeterStyles"></div>
  `,
})
export class BpProgressBarComponent {
  public static MIN_PERCENT = 0;
  public static MAX_PERCENT = 100;

  public Classes = Classes;

  private _percent: number = null;

  get bpPercent(): number {
    return this._percent;
  }

  /**
   * Progress percent
   * Min 0
   * Max 100
   */
  @Input() set bpPercent(value: number) {
    if (!value && value !== 0) {
      this._percent = null;
      return;
    }

    const { MIN_PERCENT, MAX_PERCENT } = BpProgressBarComponent;

    if (value < MIN_PERCENT) {
      this._percent = MIN_PERCENT;
    } else if (value > MAX_PERCENT) {
      this._percent = MAX_PERCENT;
    } else {
      this._percent = value;
    }
  }

  /**
   * Visual intent color to apply to element.
   */
  @Input() bpIntent: Intent;

  /**
   * Whether the background should be striped.
   */
  @Input() @InputBoolean() bpStripes = true;

  /**
   * Whether the background should animate.
   */
  @Input() @InputBoolean() bpAnimate = true;

  @HostBinding('class')
  get hostClasses(): string {
    return cx(Classes.PROGRESS_BAR, Classes.intentClass(this.bpIntent), {
      [Classes.PROGRESS_NO_ANIMATION]: !this.bpAnimate,
      [Classes.PROGRESS_NO_STRIPES]: !this.bpStripes,
    });
  }

  get progressMeterStyles(): { [key: string]: string } {
    if (!this.bpPercent && this.bpPercent !== 0) {
      return null;
    }

    return {
      width: `${this.bpPercent}%`,
    };
  }
}
