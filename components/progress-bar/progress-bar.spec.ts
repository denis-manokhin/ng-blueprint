import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { Classes, Intent } from 'ng-blueprint/core/css';

import { BpProgressBarComponent } from './progress-bar.component';

describe('BpProgressBarComponent', () => {
  let spectator: Spectator<BpProgressBarComponent>;
  const createComponent = createComponentFactory({
    component: BpProgressBarComponent,
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('renders component with default config', () => {
    const { classList } = spectator.element;

    expect(classList.contains(Classes.PROGRESS_BAR)).toBeTrue();
    expect(classList.contains(Classes.PROGRESS_NO_STRIPES)).toBeFalse();
    expect(classList.contains(Classes.PROGRESS_NO_ANIMATION)).toBeFalse();

    expect(spectator.query(`.${Classes.PROGRESS_METER}`)).toBeTruthy();
  });

  it('renders intent class', () => {
    spectator.component.bpIntent = Intent.PRIMARY;
    spectator.detectChanges();

    expect(
      spectator.element.classList.contains(Classes.INTENT_PRIMARY),
    ).toBeTrue();
  });

  it('applies width', () => {
    const progressMeter: HTMLDivElement = spectator.query(
      `.${Classes.PROGRESS_METER}`,
    );
    expect(progressMeter.style.width).toBeFalsy();

    spectator.component.bpPercent = 53;
    spectator.detectComponentChanges();

    expect(progressMeter.style.width).toBe('53%');
  });

  it('handles zero value correctly', () => {
    const progressMeter: HTMLDivElement = spectator.query(
      `.${Classes.PROGRESS_METER}`,
    );

    spectator.component.bpPercent = 0;
    spectator.detectComponentChanges();

    expect(progressMeter.style.width).toBe('0%');
  });

  it('handles invalid values correctly', () => {
    const { MIN_PERCENT, MAX_PERCENT } = BpProgressBarComponent;
    const progressMeter: HTMLDivElement = spectator.query(
      `.${Classes.PROGRESS_METER}`,
    );

    spectator.component.bpPercent = undefined;
    spectator.detectComponentChanges();
    expect(progressMeter.style.width).toBeFalsy();

    spectator.component.bpPercent = -10;
    spectator.detectComponentChanges();
    expect(progressMeter.style.width).toBe(`${MIN_PERCENT}%`);

    spectator.component.bpPercent = 110;
    spectator.detectComponentChanges();
    expect(progressMeter.style.width).toBe(`${MAX_PERCENT}%`);
  });
});
