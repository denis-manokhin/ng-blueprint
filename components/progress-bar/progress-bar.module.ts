import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BpProgressBarComponent } from './progress-bar.component';

@NgModule({
  imports: [CommonModule],
  declarations: [BpProgressBarComponent],
  exports: [BpProgressBarComponent],
})
export class BpProgressBarModule {}
