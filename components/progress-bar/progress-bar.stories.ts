import { withKnobs, number, boolean, select } from '@storybook/addon-knobs';
import { Intent } from 'ng-blueprint/core/css';
import { BpProgressBarComponent } from './progress-bar.component';

export default {
  title: 'Progress Bar',
  decorators: [withKnobs],
};

export const Default = () => ({
  component: BpProgressBarComponent,
  moduleMetadata: {
    declarations: [BpProgressBarComponent],
  },
  props: {
    intent: select('Intent', Intent, Intent.NONE),
    animate: boolean('Animate', true),
    stripes: boolean('Stripes', true),
    knownValue: boolean('Known Value', false),
    value: number('Value', 0, {
      range: true,
      min: 0,
      max: 100,
      step: 1,
    }),
  },
  template: `
    <bp-progress-bar
      bpIntent="{{ intent }}"
      bpAnimate="{{ animate }}"
      bpStripes="{{ stripes }}"
      bpPercent="{{ knownValue ? value : null }}"
    ></bp-progress-bar>
  `,
});
