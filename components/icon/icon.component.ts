import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { IconName } from '@blueprintjs/core';
import { IconSvgPaths16, IconSvgPaths20 } from '@blueprintjs/icons';
import cx from 'classnames';
import { Classes, Intent } from 'ng-blueprint/core/css';

@Component({
  selector: 'bp-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  template: `
    <svg
      *ngIf="bpIcon"
      [attr.fill]="bpColor"
      [attr.width]="bpIconSize"
      [attr.height]="bpIconSize"
      [attr.viewBox]="viewBox"
    >
      <desc *ngIf="iconTitle">{{ iconTitle }}</desc>
      <path
        *ngFor="let path of pathStrings"
        [attr.d]="path"
        fill-rule="evenodd"
      ></path>
    </svg>
  `,
})
export class BpIconComponent {
  public static SIZE_STANDARD = 16;
  public static SIZE_LARGE = 20;

  public viewBox: string;
  public pathStrings: string[] = [];

  private _icon: IconName;
  private _iconSize: number;
  private _pixelGridSize: number;

  /**
   * Name of a Blueprint UI icon.
   * This prop is required because it determines the content of the component,
   * but it can be explicitly set to falsy values to render nothing.
   */
  @Input() set bpIcon(icon: IconName) {
    this._icon = icon;
    this._updateSvgPaths();
  }

  get bpIcon(): IconName {
    return this._icon;
  }

  /**
   * Color of icon. This is used as the `fill` attribute on the `<svg>` image
   * so it will override any CSS `color` property, including that set by
   * `intent`. If this prop is omitted, icon color is inherited from
   * surrounding text.
   */
  @Input() bpColor: string;

  /**
   * Size of the icon, in pixels. Blueprint contains 16px and 20px SVG icon
   * images, and chooses the appropriate resolution based on this prop.
   */
  @Input() set bpIconSize(size: number) {
    this._pixelGridSize =
      size >= BpIconComponent.SIZE_LARGE
        ? BpIconComponent.SIZE_LARGE
        : BpIconComponent.SIZE_STANDARD;

    this.viewBox = `0 0 ${this._pixelGridSize} ${this._pixelGridSize}`;
    this._iconSize = size;
    this._updateSvgPaths();
  }

  get bpIconSize(): number {
    return this._iconSize;
  }

  /**
   * Description string. This string does not appear in normal browsers, but
   * it increases accessibility. For instance, screen readers will use it for
   * aural feedback. By default, this is set to the icon's name. Pass an
   * explicit falsy value to disable.
   */
  @Input() bpTitle: string;

  /**
   * Visual intent color to apply to element.
   */
  @Input() bpIntent: Intent;

  @HostBinding('class')
  get hostClasses(): string {
    return cx(Classes.ICON, Classes.intentClass(this.bpIntent));
  }

  @HostBinding('style.display')
  get hostDisplay(): string {
    return !!this.bpIcon ? 'inherit' : 'none';
  }

  get iconTitle(): string {
    return this.bpTitle || this.bpIcon;
  }

  constructor() {
    this.bpIconSize = BpIconComponent.SIZE_STANDARD;
  }

  private _updateSvgPaths(): void {
    if (!this.bpIcon) {
      this.pathStrings = [];
    }

    const svgPathsRecord =
      this._pixelGridSize === BpIconComponent.SIZE_STANDARD
        ? IconSvgPaths16
        : IconSvgPaths20;
    this.pathStrings = svgPathsRecord[this.bpIcon];
  }
}
