import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BpIconComponent } from './icon.component';

@NgModule({
  imports: [CommonModule],
  declarations: [BpIconComponent],
  exports: [BpIconComponent],
})
export class BpIconModule {}
