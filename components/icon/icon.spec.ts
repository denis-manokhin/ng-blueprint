import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { IconNames } from '@blueprintjs/icons';
import { Classes, Intent } from 'ng-blueprint/core/css';

import { BpIconComponent } from './icon.component';

describe('BpIconComponent', () => {
  let spectator: Spectator<BpIconComponent>;
  let component: BpIconComponent;

  const testIconName = IconNames.LAB_TEST;
  const createComponent = createComponentFactory(BpIconComponent);

  beforeEach(() => {
    spectator = createComponent();
    component = spectator.component;
  });

  it('iconSize=16 renders standard size', () => {
    const { SIZE_STANDARD } = BpIconComponent;

    component.bpIcon = testIconName;
    component.bpIconSize = SIZE_STANDARD;
    spectator.detectChanges();
    spectator.detectComponentChanges();

    const svgElement: HTMLElement = spectator.query('svg');
    expect(Number(svgElement.getAttribute('width'))).toBe(SIZE_STANDARD);
    expect(Number(svgElement.getAttribute('height'))).toBe(SIZE_STANDARD);
  });

  it('iconSize=20 renders large size', () => {
    const { SIZE_LARGE } = BpIconComponent;

    component.bpIcon = testIconName;
    component.bpIconSize = SIZE_LARGE;
    spectator.detectComponentChanges();

    const svgElement: SVGElement = spectator.query('svg');
    expect(Number(svgElement.getAttribute('width'))).toBe(SIZE_LARGE);
    expect(Number(svgElement.getAttribute('height'))).toBe(SIZE_LARGE);
  });

  it('undefined icon renders nothing', () => {
    component.bpIcon = undefined;
    spectator.detectComponentChanges();

    expect(spectator.element.style.display).toBe('none');
    expect(spectator.query('svg')).toBe(null);
  });

  it('renders intent class', () => {
    spectator.component.bpIntent = Intent.PRIMARY;
    spectator.detectChanges();

    expect(
      spectator.element.classList.contains(Classes.INTENT_PRIMARY),
    ).toBeTrue();
  });

  it('applies icon color', () => {
    const color = '#f2f2f2';

    spectator.component.bpIcon = testIconName;
    spectator.component.bpColor = color;
    spectator.detectComponentChanges();

    const svgElement: SVGElement = spectator.query('svg');
    expect(svgElement.getAttribute('fill')).toBe(color);
  });

  it('title sets content of desc element', () => {
    const title = 'test title';

    spectator.component.bpIcon = testIconName;
    spectator.component.bpTitle = title;
    spectator.detectComponentChanges();

    expect(spectator.query('desc').textContent).toBe(title);
  });

  it('applies icon name to desc by default', () => {
    spectator.component.bpIcon = testIconName;
    spectator.detectComponentChanges();

    expect(spectator.query('desc').textContent).toBe(testIconName);
  });

  it('unknown icon name renders blank icon', () => {
    // tslint:disable-next-line:no-any
    spectator.component.bpIcon = 'unknown' as any;
    spectator.detectComponentChanges();

    expect(spectator.queryAll('path').length).toBe(0);
  });
});
