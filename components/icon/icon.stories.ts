import {
  withKnobs,
  number,
  select,
  color,
  boolean,
} from '@storybook/addon-knobs';
import { Intent } from 'ng-blueprint/core/css';
import { BpIconComponent } from './icon.component';
import { IconNames } from '@blueprintjs/icons';

export default {
  title: 'Icon',
  decorators: [withKnobs],
};

export const Default = () => ({
  component: BpIconComponent,
  moduleMetadata: {
    declarations: [BpIconComponent],
  },
  props: {
    icon: select('Icon', IconNames, IconNames.LAB_TEST),
    size: number('Size', 60, {
      range: true,
      min: 0,
      max: 100,
      step: 1,
    }),
    intent: select('Intent', Intent, Intent.NONE),
    customColor: boolean('Custom color', false),
    color: color('Color', '#000'),
  },
  template: `
    <bp-icon
      bpIcon="{{ icon }}"
      bpIntent="{{ intent }}"
      bpIconSize="{{ size }}"
      bpColor="{{  customColor ? color : null }}"
    ></bp-icon>
  `,
});
