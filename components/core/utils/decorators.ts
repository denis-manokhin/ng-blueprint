/**
 * MIT License
 * Part of this code is taken from ng-zorro-antd - https://github.com/NG-ZORRO/ng-zorro-antd
 * File link - https://github.com/NG-ZORRO/ng-zorro-antd/blob/master/components/core/util/convert.ts
 */
import {
  coerceBooleanProperty,
  coerceCssPixelValue,
  _isNumberValue,
} from '@angular/cdk/coercion';

function propDecoratorFactory<T, D>(
  name: string,
  fallback: (v: T) => D,
): (target: unknown, propName: string) => void {
  function propDecorator(
    target: unknown,
    propName: string,
    originalDescriptor?: TypedPropertyDescriptor<unknown>,
  ): unknown {
    const privatePropName = `$$__${propName}`;

    if (Object.prototype.hasOwnProperty.call(target, privatePropName)) {
      window.console.warn(
        `The prop "${privatePropName}" is already exist, it will be overrided by ${name} decorator.`,
      );
    }

    Object.defineProperty(target, privatePropName, {
      configurable: true,
      writable: true,
    });

    return {
      get(): string {
        return originalDescriptor && originalDescriptor.get
          ? originalDescriptor.get.bind(this)()
          : // @ts-ignore
            this[privatePropName];
      },
      set(value: T): void {
        if (originalDescriptor && originalDescriptor.set) {
          originalDescriptor.set.bind(this)(fallback(value));
        }
        // @ts-ignore
        this[privatePropName] = fallback(value);
      },
    };
  }

  return propDecorator;
}

export function toBoolean(value: boolean | string): boolean {
  return coerceBooleanProperty(value);
}

export function toNumber(value: number | string): number;
export function toNumber<D>(value: number | string, fallback: D): number | D;
export function toNumber(
  value: number | string,
  fallbackValue: number = 0,
): number {
  return _isNumberValue(value) ? Number(value) : fallbackValue;
}

export function toCssPixel(value: number | string): string {
  return coerceCssPixelValue(value);
}

// tslint:disable-next-line:no-any
export function InputBoolean(): any {
  return propDecoratorFactory('InputBoolean', toBoolean);
}

// tslint:disable-next-line:no-any
export function InputCssPixel(): any {
  return propDecoratorFactory('InputCssPixel', toCssPixel);
}

// tslint:disable-next-line:no-any
export function InputNumber(fallbackValue?: any): any {
  return propDecoratorFactory('InputNumber', (value: string | number) =>
    toNumber(value, fallbackValue),
  );
}
